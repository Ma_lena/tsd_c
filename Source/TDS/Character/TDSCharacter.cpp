// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "TDS/FuncLibrary/Types.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "TDS/WeaponDefault.h"
#include "TDS/Character/MyCharacterMovementComponent.h"
#include "TDS/Game/TDSGameInstance.h"


DEFINE_LOG_CATEGORY_STATIC(LogCharacter, All, All);

ATDSCharacter::ATDSCharacter(const FObjectInitializer& ObjInit) : Super(ObjInit.SetDefaultSubobjectClass<UMyCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = true;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location // 10.04
	/*CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());*/

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	UpdateMouseAim(DeltaSeconds); //cursor ot Il`i !!!!!!
	/*if (CurrentCursor) //zelenaya hren`
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC)
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	} */

	
	/*if (CursorToWorld != nullptr) //////!!!!!!
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}*/
	MovementTick(DeltaSeconds);
	
}

void ATDSCharacter::BeginPlay()
{
	Super::BeginPlay();

	InitWeapon(InitWeaponName);

	/*if (CursorMaterial) //10.04
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}	*/
	
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATDSCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATDSCharacter::InputAxisY);

	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATDSCharacter::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATDSCharacter::InputAttackReleased);
	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATDSCharacter::TryReloadWeapon);

}

bool ATDSCharacter::IsSprint() const
{
	return SprintRunEnabled && bIsMovingForvard && !GetVelocity().IsZero();
	return false;
}

void ATDSCharacter::InputAxisY(float Value)
{
	AxisY = Value;
	UE_LOG(LogCharacter, Display, TEXT("dsgfmhlkah"));
}

void ATDSCharacter::InputAxisX(float Value)
{
	AxisX = Value;
	UE_LOG(LogCharacter, Display, TEXT("dsgfmhlkah"));
}

void ATDSCharacter::InputAttackPressed()
{
	AttackCharEvent(true);
}

void ATDSCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}


void ATDSCharacter::MovementTick(float DeltaTime)
{
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

	// izmeneniya 10.04
	if (MovementState == EMovementState::SprintRun_State)
	{
		FVector myRotationVector = FVector(AxisX,AxisY,0.0f);
		FRotator myRotator = myRotationVector.ToOrientationRotator();
		SetActorRotation((FQuat(myRotator)));
	}
	else
	{
		APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (myController)
		{
			FHitResult ResultHit;
			//myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);// bug was here Config\DefaultEngine.Ini
			myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);

			//float FindRotaterResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
			//SetActorRotation(FQuat(FRotator(0.0f, FindRotaterResultYaw, 0.0f)));

			if (CurrentWeapon)
			{
				FVector Displacement = FVector(0);
				switch (MovementState)
				{
				case EMovementState::Aim_State:
					Displacement = FVector(0.0f, 0.0f, 160.0f);
					CurrentWeapon->ShouldReduceDispersion = true;
					break;
				case EMovementState::AimWalk_State:
					CurrentWeapon->ShouldReduceDispersion = true;
					Displacement = FVector(0.0f, 0.0f, 160.0f);
					break;
				case EMovementState::Walk_State:
					Displacement = FVector(0.0f, 0.0f, 120.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::Run_State:
					Displacement = FVector(0.0f, 0.0f, 120.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::SprintRun_State:
					break;
				default:
					break;
				}
				
				CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
				//aim cursor like 3d Widget?
			}
		}
	}		
	
}

void ATDSCharacter::AttackCharEvent(bool bIsFiring) //nazatie na kurok
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if(myWeapon)
	{
		myWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::AttackCharEvent - CurrentWeapon - NULL"));
}

void ATDSCharacter ::CharacterUpdate()
{
	float ResSpeed = 600.0f;
	//switch (MovementState)
	//{
	if (MovementState == EMovementState::Aim_State)
	{
		ResSpeed = MovementInfo.AimSpeed;
		//break;
	}
	/*case*/else if (MovementState == EMovementState::AimWalk_State)
	{
		ResSpeed = MovementInfo.AimSpeedWalk;
		//break;
	}
	/*case*/else if (MovementState == EMovementState::Walk_State)
	{
		ResSpeed = MovementInfo.WalkSpeed;
		//break;
	}
	/*case*/else if (MovementState == EMovementState::Run_State)
	{
		ResSpeed = MovementInfo.Speed;
		//break;
	}
	/*case*/else if (MovementState == EMovementState::SprintRun_State && bIsMovingForvard)
	{
		ResSpeed = MovementInfo.SprintRun;

		//break;
	}
	//default:
	//break;
	//}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATDSCharacter::ChangeMovementState() //EMovementState NewMovementState //Pricelivanie
{
	if(!WalkEnabled && !SprintRunEnabled &&!AimEnabled)
	{
		MovementState = EMovementState::Run_State;
	}
	else
	{
		if(SprintRunEnabled)
		{
			WalkEnabled = false;
			AimEnabled = false;
			
			MovementState = EMovementState::SprintRun_State;
		}
		if(!WalkEnabled && !SprintRunEnabled &&!AimEnabled)
		{
			MovementState = EMovementState::Aim_State;
		}
		else
		{
			if(!WalkEnabled && !SprintRunEnabled &&!AimEnabled)
			{
				MovementState = EMovementState::Walk_State;
			}
			else
			{
				if(!WalkEnabled && !SprintRunEnabled &&!AimEnabled)
				{
					MovementState = EMovementState::Aim_State;
				}
			}
		}
	}
	//MovementState = NewMovementState;
	CharacterUpdate();

	//Oruzie pricelivanie izmenenia
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if(myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState);
	}
}

AWeaponDefault* ATDSCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}
void ATDSCharacter::InitWeapon(FName IdWeaponName /*FAddicionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon*/)
{
	if(CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}
	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if(myGI)
	{
		if(myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
            		FVector SpawnLocation = FVector(0);
            		FRotator SpawnRotation = FRotator(0);
            
            		FActorSpawnParameters SpawnParams;
            		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
            		SpawnParams.Owner = GetOwner();
            		SpawnParams.Instigator = GetInstigator();
            
            		AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
            		if (myWeapon)
            		{
            			FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
            			myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
            			CurrentWeapon = myWeapon;
                        			
            			myWeapon->WeaponSetting = myWeaponInfo;
            			myWeapon->WeaponInfo.Round = myWeaponInfo.MaxRound;
            			myWeapon->ReloadTime = myWeaponInfo.ReloadTime; //Debug !!!!!!!!!!!!
            			myWeapon->UpdateStateWeapon(MovementState);
            			myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATDSCharacter::WeaponReloadStart);
            			myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATDSCharacter::WeaponReloadEnd);
            			//myWeapon->OnWeaponFireStart.AddDynamic(this, &ATPSCharacter::WeaponFireStart);
            		}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}
	
	
}

void ATDSCharacter::TryReloadWeapon()
{
	if(CurrentWeapon)
	{
		if(CurrentWeapon->GetWeaponRound() <= CurrentWeapon->WeaponSetting.MaxRound)
			CurrentWeapon->InitReload();
	}
}

void ATDSCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void ATDSCharacter::WeaponReloadEnd()
{
	WeaponReloadEnd_BP();
}

void ATDSCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	WeaponFireStart_BP(Anim);
}

void ATDSCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

UDecalComponent* ATDSCharacter::GetCursorToWorld()
{
	return CurrentCursor; //10.04
}

void ATDSCharacter::WeaponReloadEnd_BP_Implementation()
{
	// in BP
}

void ATDSCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

FVector2D ATDSCharacter::GetMouseVelocity() //cursor ot Il`i
{
	

	float DeltaX, DeltaY = 0.0f;

	if (!PlayerControllerRef)
	{
	 return FVector2D(0);
	}

	PlayerControllerRef->GetInputMouseDelta(DeltaX, DeltaY);
	FVector2D MouseDelta = UKismetMathLibrary::MakeVector2D(DeltaX, DeltaY);
	FVector2D ReturnVector = MouseDelta / UGameplayStatics::GetWorldDeltaSeconds(GetWorld());


	return ReturnVector;
}

FRotator ATDSCharacter::GetMouseAimDirection(float DeltaSecond)//cursor ot Il`i
{
	
	FRotator PCRot = PlayerControllerRef->GetControlRotation();
	FRotator MouseRot = UKismetMathLibrary::MakeRotator(0.0f, 0.0f, UKismetMathLibrary::DegAtan2(GetMouseVelocity().X, GetMouseVelocity().Y));

	FRotator ReturnRot = FMath::RInterpTo(PCRot, MouseRot, DeltaSecond, MouseSmoothingStrenght);

	return ReturnRot;
}

bool ATDSCharacter::IsMouseAboveDeadZone()//cursor ot Il`i
{
	float Vector2dLength = UKismetMathLibrary::VSize2D(GetMouseVelocity());

	return Vector2dLength > MouseDeadZone;
}

void ATDSCharacter::UpdateMouseAim(float DeltaSecond)//cursor ot Il`i
{
	
	if (IsMouseAboveDeadZone())
	{
		PlayerControllerRef->SetControlRotation(GetMouseAimDirection(DeltaSecond));
	}
}

